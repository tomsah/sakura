/**
 * Settings for sakura owl carousel v 2
 * http://www.owlcarousel.owlgraphic.com/
 */
(function ($) {
  'use strict';

  // Bind main function as Drupal behaviour
  Drupal.behaviors.sakuraMap = {
    attach: attachMap
  };

  function attachMap(context, settings) {
    var $html = $('html', context);

    var map = {
      $map: $('[data-map]', context)
    };

    var STATE = {
      READY: 'is-map-ready'
    };
 // console.log(map.$map);


 //    function initMap() {
 //      console.log('iniMap');
 //      map.$mapv = new google.maps.Map(document.getElementById('map'), {
 //        center: {lat: -34.397, lng: 150.644},
 //        zoom: 8
 //      });
 //    }
  }
})(jQuery);
