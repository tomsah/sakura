/**
 * Settings for sakura owl carousel v 2
 * http://www.owlcarousel.owlgraphic.com/
 */
(function ($) {
  'use strict';

  // Bind main function as Drupal behaviour
  Drupal.behaviors.sakuraCarousel = {
    attach: attachCarousel
  };

  function attachCarousel(context, settings) {
    var $html = $('html', context);

    var carousel = {
      $carousel: $('[data-carousel]', context)
    };

    var STATE = {
      READY: 'is-carousel-ready'
    };

    var carouselOptions = {
      items: 1,
      loop: true,
      mouseDrag: true,
      touchDrag: true,
      nav: true,
      navText: ['', '02'],
      margin: 0,
      smartSpeed: 1000,
      onInitialized: callback,
      autoplay: true,
      autoplayHoverPause: true,

    };

    var mainCarouselOptions = $.extend({}, carouselOptions, {
      // animateOut: 'fadeOut',
      // animateIn: 'fadeIn'
    });

    carousel.$next = carousel.$carousel.find('[data-carousel-next]');

    carousel.$carousel.each(function() {
      var mainCarousel = ($(this).attr('data-carousel') === 'main');

      if(mainCarousel) {
        $(this).find('[data-carousel-container]').owlCarousel(mainCarouselOptions);
      }
    });

    // when carousel is initialized
    function callback(event) {
      $('.sakura-carousel').addClass(STATE.READY);
    }

    // Make sure nav is tirggering only its related carousel
    carousel.$next.click(function(e) {
      $(this).parents('[data-carousel]').find('[data-carousel-container]').trigger('next.owl.carousel');
      e.preventDefault();
    });

    var owl = $($('[data-carousel="main"]')).find('.owl-carousel');
    var navPrev = owl.find('.owl-prev');
    var $navPrevText = $('<div class="nav-prev-text">01</div>');
    navPrev.append($navPrevText);
    var navNext = owl.find('.owl-next');



    //add data attribute to onav element
    navPrev.attr('data-text', "01");
    navNext.attr('data-text', "02");

    owl.on('initialized.owl.carousel changed.owl.carousel', function(event) {
      var pages     = event.page.count;     // Number of pages
      var currentItem = event.page.index + 1;// Position of the current page
      var nextItem = currentItem + 1; // next Item
      if(nextItem === pages + 1) {
        nextItem = "1";
      }
      //change nav text and data attribute
      $navPrevText.text("0"+currentItem);
      navPrev.attr('data-text', currentItem);
      navNext.text("0"+nextItem).attr('data-text', nextItem);

    });
  }
})(jQuery);
