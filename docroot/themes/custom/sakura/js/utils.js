(function ($) {
  'use strict';

  // Bind main function as Drupal behaviour
  Drupal.behaviors.catchUtils = {
    attach: attachUtils
  };

  function attachUtils(context, settings) {
    var $html = $('html', context);

    Drupal.catch = Drupal.catch || {};

    Drupal.catch.getViewport = getViewport;
    Drupal.catch.underlineLink = underlineLink;

    function getViewport() {
      return $('[data-viewport]:visible:last').attr('data-viewport');
    }

    //calculate width and left offset for the underline
    function underlineLink ($menu, $link) {
      // We need a link and a menu
      if (! $menu.length || ! $link.length) {
        return;
      }

      var $submenu = $link.parents($menu);
      var $underline = $submenu.find('.menu-underline');

      var dimensions = {
        width: $link.outerWidth(),
        offset: $link.offset().left - $menu.offset().left
      };

      $underline.css({
        width: dimensions.width + 'px',
        transform: 'translateX(' + dimensions.offset + 'px)'
      });
    }
  }
})(jQuery);
