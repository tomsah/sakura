(function ($) {
  'use strict';

  var menuToggleActivated = false;

  var STATE = {
    OPEN: 'open',
    OUT: 'submenu-out'
  };

  // Bind main function as Drupal behaviour
  Drupal.behaviors.catchMenuToggle = {
    attach: attachMenuToggle
  };

  Drupal.catch = Drupal.catch || {};

  Drupal.catch.toggleMenu = toggleMenu;

  function attachMenuToggle(context, settings) {
    // We don't want to bind this toggle state more than once
    if (menuToggleActivated) {
      return;
    }

    $(document).on('click', '[data-menu-toggle-trigger]', function(e) {
      var targetSelector = $(this).attr('data-menu-toggle-trigger');
      var $target = $(this).parents('[data-menu-toggle]');

      if (targetSelector) {
        $target = $target.find(targetSelector);
      }

      toggleMenu($target);

      e.preventDefault();
    });

    menuToggleActivated = true;
  }

  function toggleMenu($target) {
    if($target.hasClass(STATE.OPEN)) {
      $target.addClass(STATE.OUT);

      $('[data-cs-button]').removeClass(STATE.OPEN);

      setTimeout(function() {
        $target.removeClass([STATE.OPEN, STATE.OUT].join(' '));
      }, 250);

      return;
    }

    $target.addClass(STATE.OPEN);

    $('[data-cs-button]').addClass(STATE.OPEN);
  }
})(jQuery);
