(function ($) {
  'use strict';

  var STATE = {
    MENU_OPEN: 'is-menu-open',
    MENU_CLOSE: 'is-menu-close',
    LINK_ACTIVE: 'is-link-active'
  };

  // Bind main function as Drupal behaviour
  Drupal.behaviors.sakuraMenu = {
    attach: attachMenu,
  };



  function attachMenu(context, settings) {
    var $html = $('html', context);
    var $menu = $('[data-menu]');
    var $cloneMenu = $menu.find('[data-menu-content]');
    var $menuTrigger = $('[data-menu-trigger]');
    var $clone = $('[data-menu-content]');
    var $menuNav = $('[dtat-menu-nav]');


    // divide content in column using columnzie
    $('[data-menu-main]').columnize({columns: 2 });

    //create the Nav on load
    console.log($menuTrigger);
    $.each($menuTrigger,function(index, value){
       var previous = $($menuTrigger[index - 1]).text();
       var next = $($menuTrigger[index + 1]).text();
       console.log('this', value, 'prev', previous,'next', next);
       var $previousLink = $('[data-menu-previous]');
       var $nextLink = $('[data-menu-next]');
       $previousLink.text(previous);
       $nextLink.text(next);
    });


    $menuTrigger.on('click', function(e){
      e.preventDefault();
      var $activeLink = $(this);

      //create horistonzal menu nav
      // var activeIndex = $menuTrigger.index($activeLink);
      // var previous = $($menuTrigger[activeIndex - 1]).text();
      // var next = $($menuTrigger[activeIndex + 1]).text();
      // var $previousLink = $('[data-menu-previous]');
      // var $nextLink = $('[data-menu-next]');

      //   $previousLink.text(previous);
      //   $nextLink.text(next);



      // get data to append to menu container
      var $cloneData = $(this).closest('li').find('[data-menu-content]').clone(true, true);

      //is the menu open?
      if($menu.hasClass(STATE.MENU_OPEN)  && $activeLink.hasClass(STATE.LINK_ACTIVE)) {
        $menu.addClass(STATE.MENU_CLOSE);

        setTimeout(function() {
          //destroy clone
          $('#cloned-menu > section').remove();

          $activeLink.removeClass(STATE.LINK_ACTIVE);
          $menu.removeClass([STATE.MENU_OPEN, STATE.MENU_CLOSE].join(' '));
        }, 650);

        return;
      }

      //If the menu is already open and click on an not active link
      //destroy the current clone
      //inject the new clone
      //remove the active state of the previous active link
      //add active state to the clicked link

      if(!$activeLink.hasClass(STATE.LINK_ACTIVE)) {
       $menuTrigger.removeClass(STATE.LINK_ACTIVE);
       $activeLink.addClass(STATE.LINK_ACTIVE);
       $('#cloned-menu > section').remove();
       $cloneData.appendTo($menu);
      }

      $menu.addClass(STATE.MENU_OPEN);
      $(this).addClass(STATE.LINK_ACTIVE);

    });

    //menu close
    var $closeMenu = $('[data-menu-close]');

    $closeMenu.on('click', function(e){
      e.preventDefault();

      setTimeout(function() {
        //destroy clone
        $('#cloned-menu > section').remove();
        $activeLink.removeClass(STATE.LINK_ACTIVE);
        $menu.removeClass([STATE.MENU_OPEN, STATE.MENU_CLOSE].join(' '));
      }, 650);
    });



  }
})(jQuery);
