<?php
/**
 * @file
 * Contains \Drupal\sakura_custom_fields\Plugin\Field\FieldFormatter\FiveTextFormatter.
 */

namespace Drupal\sakura_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'five_text' formatter.
 *
 * @FieldFormatter (
 *   id = "five_text",
 *   label = @Translation("FiveText"),
 *   field_types = {
 *     "five_text"
 *   }
 * )
 */
class FiveTextFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $value_one = $item->value_one;
      $value_two = $item->value_two;
      $value_three = $item->value_three;
      $value_four = $item->value_four;
      $value_five = $item->value_five;

      $elements[$delta] = array(
        'value_one' => $value_one,
        'value_two' => $value_two,
        'value_three' => $value_three,
        'value_four' => $value_four,
        'value_five' => $value_five,
      );
    }

    return $elements;
  }
}
