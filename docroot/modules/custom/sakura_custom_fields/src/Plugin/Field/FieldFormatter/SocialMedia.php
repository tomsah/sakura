<?php
/**
 * @file
 * Contains \Drupal\sakura_custom_fields\Plugin\Field\FieldFormatter\SocialMediaFormatter.
 */

namespace Drupal\sakura_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'social_media' formatter.
 *
 * @FieldFormatter (
 *   id = "social_media",
 *   label = @Translation("SocialMedia"),
 *   field_types = {
 *     "social_media"
 *   }
 * )
 */
class SocialMedia extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $facebook = $item->facebook;
      $linkedin = $item->linkedin;
      $youtube = $item->youtube;
      $instagram = $item->instagram;
      $twitter = $item->twitter;

      $elements[$delta] = array(
        'facebook' => $facebook,
        'linkedin' => $linkedin,
        'youtube' => $youtube,
        'instagram' => $instagram,
        'twitter' => $twitter,
      );
    }

    return $elements;
  }
}
