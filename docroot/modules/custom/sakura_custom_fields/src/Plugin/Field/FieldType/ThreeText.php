<?php

/**
 * @file
 * Contains \Drupal\sakura_custom_fields\Plugin\Field\FieldType\ThreeText.
 */

namespace Drupal\sakura_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'three_text' field type.
 *
 * @FieldType (
 *   id = "three_text",
 *   label = @Translation("ThreeText"),
 *   description = @Translation("Stores three values in one field."),
 *   default_widget = "three_text",
 *   default_formatter = "three_text"
 * )
 */
class ThreeText extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value_one' => array(
          'type' => 'varchar',
          'length' => 255,
        ),
        'value_two' => array(
          'type' => 'varchar',
          'length' => 255,
        ),
        'value_three' => array(
          'type' => 'varchar',
          'length' => 255,
        )
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value1 = $this->get('value_one')->getValue();
    $value2 = $this->get('value_two')->getValue();
    $value3 = $this->get('value_three')->getValue();
    return empty($value1) && empty($value2) && empty($value3);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['value_one'] = DataDefinition::create('string')
      ->setLabel(t('Value one'))
      ->setDescription(t('This will be displayed in the left column'));

    $properties['value_two'] = DataDefinition::create('string')
      ->setLabel(t('Value two'))
      ->setDescription(t('This will be displayed in the middle column'));

    $properties['value_three'] = DataDefinition::create('string')
      ->setLabel(t('Value three'))
      ->setDescription(t('This will be displayed in the right column'));

    return $properties;
  }
}
