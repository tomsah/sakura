<?php
/**
 * @file
 * Contains \Drupal\two_textfield\Plugin\Field\FieldWidget\TwoTextWidget.
 */

namespace Drupal\sakura_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'two_text' widget.
 *
 * @FieldWidget (
 *   id = "two_text",
 *   label = @Translation("TwoText widget"),
 *   field_types = {
 *     "two_text"
 *   }
 * )
 */
class TwoTextWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $field_name = $items->getName();

    $title_1 = t('Value one');
    $title_2 = t('Value two');
    switch($field_name) {
      case 'field_statistic':
        $title_1 = $this->t('Statistic value');
        $title_2 = $this->t('Statistic title/description');
        break;
      case 'field_sub_services':
        $title_1 = $this->t('Sub service title');
        $title_2 = $this->t('Sub service page link (leave blank if no page)');
        break;
      case 'field_service_info':
        $title_1 = $this->t('Title/Header');
        $title_2 = $this->t('Description/sub-copy');
        break;
    }
    $element['value_one'] = array(
      '#type' => 'textfield',
      '#title' => $title_1,
      '#default_value' => isset($items[$delta]->value_one) ? $items[$delta]->value_one : '',
      '#size' => 60,
    );
    $element['value_two'] = array(
      '#type' => 'textfield',
      '#title' => $title_2,
      '#default_value' => isset($items[$delta]->value_two) ? $items[$delta]->value_two : '',
      '#size' => 60,
    );

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container-inline')),
      );
    }

    return $element;
  }
}
