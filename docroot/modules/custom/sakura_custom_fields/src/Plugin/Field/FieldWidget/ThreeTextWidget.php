<?php
/**
 * @file
 * Contains \Drupal\three_textfield\Plugin\Field\FieldWidget\ThreeTextWidget.
 */

namespace Drupal\sakura_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'three_text' widget.
 *
 * @FieldWidget (
 *   id = "three_text",
 *   label = @Translation("ThreeText widget"),
 *   field_types = {
 *     "three_text"
 *   }
 * )
 */
class ThreeTextWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element['value_one'] = array(
      '#type' => 'textfield',
      '#title' => t('Value one'),
      '#default_value' => isset($items[$delta]->value_one) ? $items[$delta]->value_one : '',
      '#size' => 60,
    );

    $element['value_two'] = array(
      '#type' => 'textfield',
      '#title' => t('Value two'),
      '#default_value' => isset($items[$delta]->value_two) ? $items[$delta]->value_two : '',
      '#size' => 60,
    );

    $element['value_three'] = array(
      '#type' => 'tel',
      '#title' => t('Value three'),
      '#default_value' => isset($items[$delta]->value_three) ? $items[$delta]->value_three : '',
      '#size' => 60,
    );

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container-inline')),
      );
    }

    return $element;
  }
}
