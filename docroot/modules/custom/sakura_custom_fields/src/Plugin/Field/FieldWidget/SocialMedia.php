<?php
/**
 * @file
 * Contains \Drupal\sakura_custom_fields\Plugin\Field\FieldWidget\SocialMediaWidget.
 */

namespace Drupal\sakura_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'social_media' widget.
 *
 * @FieldWidget (
 *   id = "social_media",
 *   label = @Translation("SocialMedia widget"),
 *   field_types = {
 *     "social_media"
 *   }
 * )
 */
class SocialMedia extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element['facebook'] = array(
      '#type' => 'url',
      '#title' => t('Facebook'),
      '#default_value' => isset($items[$delta]->facebook) ? $items[$delta]->facebook : '',
      '#description' => t('URL for facebook profile. This must be an external URL such as http://example.com.'),
    );

    $element['linkedin'] = array(
      '#type' => 'url',
      '#title' => t('LinkedIn'),
      '#default_value' => isset($items[$delta]->linkedin) ? $items[$delta]->linkedin : '',
      '#description' => t('URL for LinkedIn profile. This must be an external URL such as http://example.com.'),
    );

    $element['youtube'] = array(
      '#type' => 'url',
      '#title' => t('YouTube'),
      '#default_value' => isset($items[$delta]->youtube) ? $items[$delta]->youtube : '',
      '#description' => t('URL for YouTube profile. This must be an external URL such as http://example.com.'),
    );
    $element['instagram'] = array(
      '#type' => 'url',
      '#title' => t('Instagram'),
      '#default_value' => isset($items[$delta]->instagram) ? $items[$delta]->instagram : '',
      '#description' => t('URL for Instagram profile. This must be an external URL such as http://example.com.'),
    );
    $element['twitter'] = array(
      '#type' => 'url',
      '#title' => t('Twitter'),
      '#default_value' => isset($items[$delta]->twitter) ? $items[$delta]->twitter : '',
      '#description' => t('URL for Twitter profile. This must be an external URL such as http://example.com.'),
    );
    return $element;
  }
}
