<?php
/**
 * @file
 * Contains \Drupal\five_textfield\Plugin\Field\FieldWidget\FiveTextWidget.
 */

namespace Drupal\sakura_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'five_text' widget.
 *
 * @FieldWidget (
 *   id = "five_text",
 *   label = @Translation("FiveText widget"),
 *   field_types = {
 *     "five_text"
 *   }
 * )
 */
class FiveTextWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element['value_one'] = array(
      '#type' => 'textfield',
      '#title' => t('ref item'),
      '#default_value' => isset($items[$delta]->value_one) ? $items[$delta]->value_one : '',
      '#size' => 60,
    );

    $element['value_two'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($items[$delta]->value_two) ? $items[$delta]->value_two : '',
      '#size' => 60,
    );

    $element['value_three'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($items[$delta]->value_three) ? $items[$delta]->value_three : '',
      '#size' => 60,
    );

    $element['value_four'] = array(
      '#type' => 'textfield',
      '#title' => t('Price or number of item'),
      '#default_value' => isset($items[$delta]->value_four) ? $items[$delta]->value_four : '',
      '#size' => 60,
    );

    $element['value_five'] = array(
      '#type' => 'textfield',
      '#title' => t('Price description'),
      '#default_value' => isset($items[$delta]->value_five) ? $items[$delta]->value_five : '',
      '#size' => 60,
    );

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container-inline')),
      );
    }

    return $element;
  }
}
