<?php

/**
 * @file
 * Contains \Drupal\catch_custom_fields\Plugin\Field\FieldType\SocialMedia.
 */

namespace Drupal\catch_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'social_media' field type.
 *
 * @FieldType (
 *   id = "social_media",
 *   label = @Translation("SocialMedia"),
 *   description = @Translation("Stores social media values in one field."),
 *   default_widget = "social_media",
 *   default_formatter = "social_media"
 * )
 */
class SocialMedia extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'facebook' => array(
          'type' => 'varchar',
          'length' => 2048,
        ),
        'linkedin' => array(
          'type' => 'varchar',
          'length' => 2048,
        ),
        'youtube' => array(
          'type' => 'varchar',
          'length' => 2048,
        ),
        'instagram' => array(
          'type' => 'varchar',
          'length' => 2048,
        ),
        'twitter' => array(
          'type' => 'varchar',
          'length' => 2048,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $facebook = $this->get('facebook')->getValue();
    $linkedin = $this->get('linkedin')->getValue();
    $youtube = $this->get('youtube')->getValue();
    $instagram = $this->get('instagram')->getValue();
    $twitter = $this->get('twitter')->getValue();
    return empty($facebook) && empty($linkedin) && empty($youtube) && empty($instagram) && empty($twitter);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['facebook'] = DataDefinition::create('string')
      ->setLabel(t('Facebook'))
      ->setDescription(t('URL for facebook profile'));

    $properties['linkedin'] = DataDefinition::create('string')
      ->setLabel(t('LinkedIn'))
      ->setDescription(t('URL for LinkedIn profile'));

    $properties['youtube'] = DataDefinition::create('string')
      ->setLabel(t('YouTube'))
      ->setDescription(t('URL for YouTube profile'));

    $properties['instagram'] = DataDefinition::create('string')
      ->setLabel(t('Instagram'))
      ->setDescription(t('URL for Instagram profile'));

    $properties['twitter'] = DataDefinition::create('string')
      ->setLabel(t('Twitter'))
      ->setDescription(t('URL for Twitter profile'));

    return $properties;
  }
}
