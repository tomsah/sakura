<?php
/**
 * @file
 * Contains \Drupal\catch_custom_fields\Plugin\Field\FieldFormatter\TwoTextFormatter.
 */

namespace Drupal\catch_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'two_text' formatter.
 *
 * @FieldFormatter (
 *   id = "two_text",
 *   label = @Translation("TwoText"),
 *   field_types = {
 *     "two_text"
 *   }
 * )
 */
class TwoTextFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $value_one = $item->value_one;
      $value_two = $item->value_two;

      $elements[$delta] = array(
        'value_one' => $value_one,
        'value_two' => $value_two,

      );
    }

    return $elements;
  }
}
