<?php
/**
 * @file
 * Contains \Drupal\catch_custom_fields\Plugin\Field\FieldFormatter\ThreeTextFormatter.
 */

namespace Drupal\catch_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'three_text' formatter.
 *
 * @FieldFormatter (
 *   id = "three_text",
 *   label = @Translation("ThreeText"),
 *   field_types = {
 *     "three_text"
 *   }
 * )
 */
class ThreeTextFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $value_one = $item->value_one;
      $value_two = $item->value_two;
      $value_three = $item->value_three;

      $elements[$delta] = array(
        'value_one' => $value_one,
        'value_two' => $value_two,
        'value_three' => $value_three,
      );
    }

    return $elements;
  }
}
